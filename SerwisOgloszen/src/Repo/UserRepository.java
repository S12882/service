package Repo;

import java.util.*;

import org.hsqldb.util.DatabaseManager;
import org.hsqldb.util.DatabaseManagerSwing;
import org.hsqldb.server.WebServer;
import org.hsqldb.jdbc.JDBCDriver;

import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;

import unitOfWork.UnitOfWork;
import Serwis.*;



public class UserRepository implements UserRepo {
	
	
	private Connection connection;
	private ResultSet resultSet = null;
	private Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private PreparedStatement DELETEStmt;
	private String url = "jdbc:hsqldb:file:testdb"; // Here Ur Database URL Plz 

   
		public void readDataBase() throws Exception {
			try {
			     Class.forName("org.hsqldb.jdbc.JDBCDriver" );
			 } catch (Exception e) {
			     System.err.println("ERROR: failed to load HSQLDB JDBC driver.");
			     e.printStackTrace();
			     return;
			 }

			 Connection connection = DriverManager.getConnection("url");
		
			 statement = connection.createStatement();
			  resultSet = statement
			      .executeQuery("select * from serwis");
			  writeResultSet(resultSet); 
	}		

		private void writeResultSet(ResultSet resultSet) throws SQLException {
			
			while (resultSet.next()) {
			
			  String Name = resultSet.getString("Name");
			  String Cena = resultSet.getString("Cena");
			  String Size = resultSet.getString("Size");
			  String Type = resultSet.getString("Type");

			  System.out.println("Name: " + Name);
			  System.out.println("Email: " + Cena);
			  System.out.println("Address: " + Size);
			  System.out.println("Description: " + Type);

			  }
		 }

		private void close() {
		    try {
		      if (resultSet != null) {
		        resultSet.close();
		      }

		      if (statement != null) {
		        statement.close();
		      }

		      if (connection != null) {
		    	  connection.close();
		      }
		    } catch (Exception e) {

		    }
		  }

	public void add(Serwis serwis) throws ClassNotFoundException{
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			  connection = DriverManager
			      .getConnection(url);

			  statement = connection.createStatement();
			  resultSet = statement
			      .executeQuery("select * from person");
			  writeResultSet(resultSet);
			  
			  String sql = "INSERT INTO person VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
			  preparedStatement = connection.prepareStatement(sql);
			  
			      preparedStatement.setString(1, serwis.getId());
				  preparedStatement.setString(2, serwis.getName());
				  preparedStatement.setDouble(3, serwis.getCena());
				  preparedStatement.setString(4, serwis.getType());
				  preparedStatement.setString(5, serwis.getDescr());
				  preparedStatement.setString(6, serwis.getEmail());
				  preparedStatement.setString(7, serwis.getAddress());
				  preparedStatement.setInt(8, serwis.getNumber());
				  preparedStatement.executeUpdate();
				  writeResultSet(resultSet);
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		close();
	}


	public void modify(Serwis entity) {
			
	}
	
	public boolean checkEmail(Serwis serwis) throws ClassNotFoundException{
		boolean check = true;
		try {		
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			  connection = DriverManager
			      .getConnection(url);

			  statement = connection.createStatement();
			  resultSet = statement
			      .executeQuery("select * from person");
			  writeResultSet(resultSet);
		
		String queryCheck = "SELECT email from serwis WHERE email = '? '";
		 preparedStatement = connection.prepareStatement(queryCheck);
		 preparedStatement.setString(1, serwis.getEmail());
		 writeResultSet(resultSet);
		 if(!resultSet.next()) {
			 check = false;
	
        }
		 
		} catch (SQLException e) {
			e.printStackTrace();
		}
		close();
		return check;
	}
	
	public boolean checkId(Serwis serwis) throws ClassNotFoundException{
		boolean check = true;
		try {		
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			  connection = DriverManager
			      .getConnection(url);

			  statement = connection.createStatement();
			  resultSet = statement
			      .executeQuery("select * from person");
			  writeResultSet(resultSet);
		
		String queryCheck = "SELECT email from serwis WHERE email = '? '";
		 preparedStatement = connection.prepareStatement(queryCheck);
		 preparedStatement.setString(1, serwis.getId());
		 writeResultSet(resultSet);
		 if(!resultSet.next()) {
			 check = false;
        }
		 
		} catch (SQLException e) {
			e.printStackTrace();
		}
		close();
		return check;
	}

	
	public void delete(Serwis serwis) {
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			  connection = DriverManager
			      .getConnection(url);

			  statement = connection.createStatement();
			  resultSet = statement
			      .executeQuery("select * from person");
			  writeResultSet(resultSet);
			
			String delete = "Delete from serwis WHERE id = '? '";
			 preparedStatement = connection.prepareStatement(delete);
			 preparedStatement.setString(1, serwis.getId());
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void Search(Serwis serwis){
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			  connection = DriverManager
			      .getConnection(url);

			  statement = connection.createStatement();
			  resultSet = statement
			      .executeQuery("select * from person");
			  writeResultSet(resultSet);
			  if(serwis.getName() != null){
			  String search = "select * from serwis where name=' ? ' ";
			  preparedStatement = connection.prepareStatement(search);
			  preparedStatement.setString(1, serwis.getName());
			  }
			  		  
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	
	
	public void count() {
		
	}


	public Serwis withName(String name) {

		return null;
	}


	public Object getAll() {
	
		return null;
	}


	@Override
	public void save(Serwis serwis) {
	
		
	}
	
	public String getUrl(){
		return url;
	}


	
	
}

