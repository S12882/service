package Repo;

import java.sql.Connection;

import unitOfWork.*;
import Serwis.*;

public class RepositoryCatalog implements RepoCatalog {

	private Connection connection;
	private UnitOfWork Unit;

	public RepositoryCatalog(Connection connection, UnitOfWork uow){
		super();
		this.connection = connection;
		this.Unit = uow;
	}
	
	public UserRepo getUsers() {
		return new UserRepository();
	}

	public void commit() {
		Unit.commit();
	}

	public static RepoCatalog catalog() {
	
		
		return null;
	}

	@Override
	public UserRepo getAll() {
		
		return null;
	}

	@Override
	public UserRepo getSerwis() {
		
		return null;
	}

}
