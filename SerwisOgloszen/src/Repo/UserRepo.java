package Repo;

import java.util.*;

import Serwis.*;

public interface UserRepo extends Repository<Serwis> {

	public Serwis withName(String name);
	public void save(Serwis user);
	public Object getAll();

	
}
