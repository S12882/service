package Repo;

import java.util.*;

import Serwis.*;

public interface RepoCatalog {
	

	public void commit();
	public UserRepo getSerwis();
	public UserRepo getUsers();
	public UserRepo getAll();
	
	
}


