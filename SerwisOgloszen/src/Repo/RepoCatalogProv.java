package Repo;


import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.SQLException;
import unitOfWork.UnitOfWork;
import unitOfWork.cUnitOfWork;

public class RepoCatalogProv {


	private static String url="jdbc:hsqldb:hsql://localhost/workdb";
	
	public static RepoCatalog catalog()
	{

		try {
			Connection connection = DriverManager.getConnection(url);
			UnitOfWork uow = new cUnitOfWork(connection);
			RepoCatalog catalog = new RepositoryCatalog(connection, uow);
		
			return catalog;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}