package Serwis;



public abstract class Entity {
 
	private String id;
	EntityState state;
	
	public String getId(){
		return id;
	}
	
	public EntityState getState(){
		return state;
	}
	
	public void setId(String id){
		this.id = id;
	}
	
	public void setState(EntityState state){
		this.state = state;
	}
	
}
