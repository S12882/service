package Serwis;

import java.util.*;

public class UserRoles extends Entity {
	
	private String roleId;
  
    private List<User> users;
    private List<RolesPermissions> permissions;
    
    public UserRoles(){
    	users = new ArrayList<User>();
    	permissions = new ArrayList<RolesPermissions>();
    }
    
    public String getId() {
		return roleId;
	}
	public void setId(String roleId) {
		this.roleId = roleId;
	}
	public List<RolesPermissions> getPrivileges() {
		return permissions;
	}
	public void setPrivileges(List<RolesPermissions> permissions) {
		this.permissions = permissions;
	}
	public List<User> getUsers() {
		return users;
	}
	public void setUsers(List<User> users) {
		this.users = users;
	}
}
