package Serwis;

public enum Type {apartment ("apartment"), land ("land"), house ("house"), shop("shop"), estate("estate");
	
	 private final String name;       

private Type(String s) {
    name = s;
}

public boolean equalsName(String otherName) {
    return (otherName == null) ? false : name.equals(otherName);
}

public String toString() {
   return this.name;
}
}
