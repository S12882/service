package Serwis;

import java.util.UUID;


public class Serwis extends Entity {
	
	String name;
	String descr;
	String address;
	String email;
	String idOgloszenia;
	double cena;
	int phoneNumber;
	int size;

	Type type;
	
public String getName(){
	return name;
}

public void setName(String name){
	this.name = name;
}

public String getDescr(){
	return descr;
}

public void setDescr(String descr){
	this.descr = descr; 
}

public String getAddress(){
	return address;
}

public void setAddress(String email){
	this.email = email; 
}

public String getEmail(){
	return email;
}

public void setEmail(String email){
	this.email = email; 
}

public double getCena(){
	return cena;
}

public void setCena(double cena){
	this.cena = cena; 
}

public int getNumber(){
	return phoneNumber;
}

public void setNumber(int phoneNumber){
	this.phoneNumber = phoneNumber; 
}

public int getSize(){
	return size;
}

public void setSize(int size){
	this.size = size; 
}

public String getId(){
	return idOgloszenia;
}

public void setId(){
	String uuid = UUID.randomUUID().toString();
	this.idOgloszenia = uuid; 
}

public String getType(){
	String s = this.type.toString();
	return s;
}

public void setType(Type type){
	this.type = type;
}

}
