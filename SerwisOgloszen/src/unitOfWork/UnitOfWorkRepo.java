package unitOfWork;

import Serwis.Entity;

public interface UnitOfWorkRepo {

	public void persistAdd(Entity entity);
	public void persistUpdate(Entity entity);
	public void persistDelete(Entity entity);
}
