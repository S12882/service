package unitOfWork;


import Serwis.Entity;

public interface UnitOfWork {

	public void commit();
	public void rollback();
	public void markAsNew(Entity entity, UnitOfWorkRepo repository);
	public void markAsDirty(Entity entity, UnitOfWorkRepo repository);
	public void markAsDeleted(Entity entity, UnitOfWorkRepo repository);
}